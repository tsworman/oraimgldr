# README #
This project includes two python 3.x files that allow for reading and writing binary or text files as blobs/clobs to an oracle database using cx_Oracle.

Install Python 3.x

Install Oracle client

Install cx_Oracle

Edit either the reader or the loader file to have your credentials in the connection string.

Edit the insert or select statement in each to match your table.


Storage parameters on my table were:
PCTFREE 1

INITRANS 64

TABLESPACE mytabspacehere

NOLOGGING

COMPRESS    

lob (fieldnamehere) store as securefile (

  tablespace mytabspacehere enable storage in row chunk 8192

  nocache nologging nocompress keep_duplicates)

;

### Who do I talk to? ###
Tyler Worman (tsworman@novaslp.net)