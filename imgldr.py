import cx_Oracle
import os

conn = cx_Oracle.connect('user/password@databaseSID')
sql = 'insert into table_blobstore (id_key, image_data, file_type) values (:id_key, :image_data, :file_type)'
imagePath = '/home/tworman/share/'

##Get file and first part is name, second is path.
for dirname, dirnames, filenames in os.walk(imagePath):
    for filename in filenames:
        basename, extension = os.path.splitext(filename)

        cursor = conn.cursor()
        id_key = basename
        file_type = extension
        
        with open(os.path.join(dirname,filename), "rb") as f:
            image_data = f.read()

        blobvar = cursor.var(cx_Oracle.CLOB)
        blobvar.setvalue(0, image_data)
        cursor.execute(sql, id_key=id_key, image_data=blobvar, file_type=file_type)
        conn.commit()
        cursor.close()

conn.close()
